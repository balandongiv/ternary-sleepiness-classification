classdef CmmProc
    
    properties
    end
    
    methods (Static)
        
        
        function ev=DatasetDivision (ev)
        %% Validated with manual inspection in excel(23 Nov 2017)
        
        
        %%% The column for the combine feature are, Row No of each of the feature,
        %%% Sleep quality (%), KSS, PRM, and PVT.
        combine_Feature=[(1:size(ev.state.kss,1))', ...
            ev.state.sq_percent,        ...
            ev.state.kss,               ...
            table2array(ev.table.prm),  ...
            table2array(ev.table.pvt),  ...
            ev.trialNo];
        
        %% To understand, please refer the 4 lines below
        % Dataset=sort (vertcat (Dataset_cell{:,:}));
        % Logical_res=all((~isnan(combine_Feature)),2);
        %  Validtn_Loc=intersect(find(Logical_res==1),Dataset); % Intersection between the NoN Nan row of features & the selected dataset (e.g., 6hE,6hL, 8h)
        %  New=combine_Feature(Validtn_Loc,:);
        %%% For compactness, use the single lines below. The output are same as
        %%% those in combine feature. However, this one is the filtered version,
        %%% which are, there is No NaN row, and only row that we interested>- For
        %%% example row for group 6hE & 6HL, or row of 8h only, or any combination
        %%% of the shift schedule protocol
        
        
        Dataset_cell= table2array(ev.Table_location(:,ev.cnd.SD));
        nDim=size(Dataset_cell,1);
        %%%% The Var Dataset_F (f:Filtered), is a cleanup Dataset ( remove row that have either one or more NaN along the Row)
        Dataset_F=single(combine_Feature((intersect(find((all((~isnan(combine_Feature)),2))==1),sort (vertcat (Dataset_cell{:,:})))),:));
        ExtractedData = (arrayfun(@(k)  Dataset_cell(k,:),1:size(Dataset_cell,1),'un',0))';
        %%% UF:Unfiltered> location of the validation set in variable ValidationSet_cell_UF is in cell,
        ValidationSet_cell_UF = (arrayfun( @(m) cat(1, ExtractedData{m}{:}),1:numel(ExtractedData),'un',0));
        
        ev.dataset.ValidationSet_cell= arrayfun(@(x)find(all(ismember(Dataset_F(:,1),ValidationSet_cell_UF{x}),2)),1:nDim,'un',0)';
        Dataset_F_idx=(1:size(Dataset_F,1))'; % Total row size of the Dataset_F
        
        
        ev.dataset.TrainingSet_cell = cellfun( @(m) setdiff(Dataset_F_idx,m),ev.dataset.ValidationSet_cell,'Un',0);
        
        %%% Sleep quality (%), KSS, PRM, and PVT (True=sleepy, False=Alert)
        [ev.state.sq_percent,ev.state.kss,ev.state.prm,ev.state.pvt,ev.trialNo_filtered]=...
            deal(Dataset_F(:,2),Dataset_F(:,3),Dataset_F(:,4),(Dataset_F(:,5:end-1)),Dataset_F(:,end));
        
        
        fields={'Table_location','table','cnd'}; % Remove this field, since no usage of it anymore
        ev=rmfield(ev,fields);
        end
            
         function [RangeFP, fnames]=forloop2ndgrid(z)
            RangeFPara = struct2cell(z) ;
            RangeFP = cell(length(RangeFPara),1); % Prelocate memory
            [RangeFP{end:-1:1}] = ndgrid(RangeFPara{:}) ;
            RangeFP = cellfun(@(x)x(:),RangeFP,'un',0);
            RangeFP = single(fliplr([RangeFP{:}]));  % The flip ensure the column arrange are KDEA,KDEF,PRMSQ00,...,PRMSQ11% s.CpTable_obs=CpTable(:,1:2);
            fnames = (fieldnames(z))';
         end
        
         
            function FileName=getNames_Type1(threshold,BaseName)
        NamesVct = sprintf('ns_%2.0f\n',threshold);
        NewName = regexp(NamesVct, '\n', 'split');
        NewName = NewName(1:end-1);

        ext='.mat';
        FileName=char(strcat(BaseName,NewName,ext));
    end
   
        
    end
end
