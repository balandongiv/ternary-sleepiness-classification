classdef SubTech
    
    properties
    end
    
    methods (Static)

        function Sq_Percentg=SleepQuality (sq_RAW)
            Sq_Percentg=(sq_RAW(:,1)+sq_RAW(:,2)+sq_RAW(:,3)+sq_RAW(:,4))./sq_RAW(:,5);
        end
        
        function [psbleKdePair] = kde_prb(ev)
            
            
            [RangeKDE, ~]=CmmProc.forloop2ndgrid(ev.KDE);
            RangeKDE(:,1+end) = sum(RangeKDE,2);
            RangeKDE=RangeKDE(any(RangeKDE(:,end)==1,2),:);
            RangeKDE=RangeKDE(:,1:3); % remove the last column, which is all one
            %%%%%%%%%%%%%%%%%%%%
            KDE_PAIR.a=1:size(RangeKDE,1);
            KDE_PAIR.b=1:size(RangeKDE,1);
            KDE_PAIR.c=1:size(RangeKDE,1);
            
            [KDE_PAIR_Xx, ~]=CmmProc.forloop2ndgrid(KDE_PAIR);
            KDE_PAIR_X=KDE_PAIR_Xx(1:round(size (KDE_PAIR_Xx,1)),:);
            
            var_kde_Xvv = arrayfun(@(k)  [RangeKDE(KDE_PAIR_X(k,1),:),RangeKDE(KDE_PAIR_X(k,2),:),RangeKDE(KDE_PAIR_X(k,3),:)],1:size(KDE_PAIR_X,1),'un',0);
            
            psbleKdePair = vertcat (var_kde_Xvv{:});
            
        end
 
        function [kdePredction]=EvalKDE (randData,ev)
            
            
            %%% 20 December 2017:
            %%% This file was used to evaluate how our system able to
            %%% mitigate if the subjective sleepiness information is not available. In
            %%% this project, the SBN assume the state of the KSS is equivalent to the
            %%% state of PRM at that particular moment, whenever if no KSS info is
            %%% available. Whereas, in typical KDE, if no kss is available, kde always
            %%% assume that it is sleepy at that particular moment.
            
            %%% The Pvt_th, Prm_th, sq_th was 6, 1.85, and 0.85, respectively.
            %%% The work shift protocol was 8h and 6 Late groups.
            
            % load('dev_kde_vary011218.mat')
            NonSlEEPY=0; MidRange=1; SLEEPY=2;
            
            % [aa,~]=size(randData);
            % prdctn_KDE=zeros(aa,1); % Prelocate memory
            for f_x=1:size(randData,2)  % This for loop lau check multiple case
                KSS_valid=randData(:,f_x);
                xx=ev.KSS_train(ev.PVT_train == SLEEPY);
                yy=ev.KSS_train(ev.PVT_train == MidRange);
                vv=ev.KSS_train(ev.PVT_train == NonSlEEPY);
                [D_Sleepy,~]  =ksdensity(xx ,KSS_valid);   % fit the empircal density to smokers
                [D_NonSleepy,~]=ksdensity(vv,KSS_valid);  % and non in turn...
                [D_MidRange,~]=ksdensity(yy,KSS_valid);  % and non in turn...
                
                % densityKDE=[D_NonSleepy,D_MidRange,D_Sleepy];
                [~,kdePredction]=max([D_NonSleepy,D_MidRange,D_Sleepy],[],2);
                % densityKDE_prob=(var_kde(:,i))';
                % prdctn_KDE(:,f_x)=i-1;
                % D_Sleepy_eta=bsxfun(@times, D_Sleepy, (KdeLr_eta.*(ones(length(D_Sleepy),1))));
                % prdctn_KDE(:,f_x)=bsxfun(@gt, D_Sleepy_eta, D_NonSleepy); % Return binary.If 1: its Sleepy, 0:Alert| Column 3 is the predicted smoking status
                
                % [m,i]=max(RangeFP,[],2);
            end
            
            % prdctn_KDE(POSI_KssNaN)=SLEEPY; % Replace NaN with 1, assume any missing as Sleepy
            
        end
        
        function cpt_val=Murphy_tb(tsd,sq,pvt)
            
            
            data_logical=[tsd';sq';pvt'];
            data_raw=double(data_logical);
            data_one=data_raw;
            data_one(data_one==2) = 3;
            data_one(data_one==1) = 2;
            data_one(data_one==0) = 1;
            data=data_one;
            N = 3;
            dag = zeros(N,N);
            TSD = 1; SQ = 2; HNode = 3;
            
            dag(SQ,HNode) = 1;
            dag(TSD,HNode)=1;
            
            
            % false = 1; true = 2;
            ns = 3*ones(1,N);  %% ns represent node_sizes, the number of values of nodes can take on
            bnet = mk_bnet(dag, ns);  %% Make a Bayesian network, makes a graphical model with an arc from i to j iff DAG(i,j) = 1.
            
            bnet.CPD{SQ} = tabular_CPD(bnet, SQ, [0.8 0.2 0.1]);
            bnet.CPD{TSD} = tabular_CPD(bnet, TSD, [0.5 0.9 0.1]);
            
            
            
            % TsdSq_FF_T=0.05;
            % TsdSq_TF_T=0.9;
            % TsdSq_FT_T=0.85;
            % TsdSq_TT_T=0.7;
            
            TsdSq_FF_T=0.05;
            TsdSq_TF_T=0.77;
            TsdSq_FT_T=0.70;
            TsdSq_TT_T=0.89;
            
            
            TsdSq_FF_F=1-TsdSq_FF_T;
            TsdSq_TF_F=1-TsdSq_TF_T;
            TsdSq_FT_F=1-TsdSq_FT_T;
            TsdSq_TT_F=1-TsdSq_TT_T;
            
            CPT_init=[TsdSq_FF_F TsdSq_TF_F TsdSq_FT_F TsdSq_TT_F...
                TsdSq_FF_T TsdSq_TF_T TsdSq_FT_T TsdSq_TT_T];
            
            % % % bnet.CPD{HNode} = tabular_CPD(bnet, HNode, CPT_init);
            bnet.CPD{HNode} = tabular_CPD(bnet, HNode);
            
            % load('dataDef.mat');
            % ncases = 100;
            % data = zeros(N, ncases);
            % for m=1:ncases
            %   data(:,m) = cell2num(sample_bnet(bnet));  %%sample_bnet returns a Nx1 cell array, data(i,j) contains the value of i node in case j
            % end
            %
            
            % data(data==2) = 0;
            
            
            %% Parameter Learning
            %Maximum likelihood parameter estimation from complete data
            %suppose we have learned the network by K2
            
            
            samples=num2cell(data); % They want the data in cell format
            %Finally, we find the maximum likelihood estimates of the parameters.
            bnet3 = learn_params(bnet, samples);
            %To view the learned parameters:
            CPT3 = cell(1,N);
            for i=1:N
                s=struct(bnet3.CPD{i});  % violate object privacy
                CPT3{i}=s.CPT;
            end
            %Here are the parameters learned for node 4.
            % dispcpt(CPT3{3}) %estimated
            
            %%% Cara CPT tu kena susun
            %%%                   P(Sleepy=False)  P(Sleepy=True)
            %%% The CPT 1:F 1:F
            %%% The CPT 2:T 1:F
            %%% The CPT 1:F 2:T
            %%% The CPT 2:T 2:T
            cpt_val=extract_CPT(CPT3{3});
            
            
            
            function cpt_val=extract_CPT(CPT)
                n = ndims(CPT);
                parents_size = size(CPT);
                % parents_size = parents_size(1:end-1);
                child_size = size(CPT,n);
                c = 1;
                vv=prod(parents_size);
                for i=1:vv
                    parent_inst = ind2subv(parents_size, i);
                    
                    index = num2cell([parent_inst 1]);
                    index{n+1} = ':';
                    xx=CPT(index{:});
                    cpt_val(i,:)=[parent_inst,xx];
                end
            end
        end
        
        function res_prm=EvalPrm_multilevel(vec_pred_prm,f_prm_th)
            
            portion=f_prm_th;
            
            bb=(vec_pred_prm-min(vec_pred_prm)).*1/(max(vec_pred_prm)-min(vec_pred_prm)); % Normalize
            
            res_prm = nan(length (vec_pred_prm),1);
            res_prm (bb <= portion(1) ) = 0;
            res_prm (bb >= portion(1) & bb <= portion(1)+ portion(2)) = 1;
            res_prm (bb >  portion(1)+ portion(2) )=2;
            
            % compare=[ bb,res_prm];
            % vec_th_PRM=f_prm_th.*(ones(length(vec_pred_prm),1)); % create a vector of TSD_threshold, so we do element-by-element binary operation
            % res_prm =bsxfun(@ge,vec_pred_prm,vec_th_PRM);  % Label as 1 (sleepy) if greater than the tsd_threshold, and 0 as alert. The predicted result put at column 3 of STAT
            %%%%%%%%%%%%%%%
            % bb=(PvtMat-min(PvtMat)).*1/(max(PvtMat)-min(PvtMat)); % Normalize
            % S = nan(length (nums),1);
            % S (bb <= portion(1) ) = 0;
            % S (bb >= portion(1) & bb <= portion(1)+ portion(2)) = 1;
            % S (bb >  portion(1)+ portion(2) )=2;
        end
        
        function res_SQ=EvalSQ_multilevel(vec_pred_sq,f_sq_rat)
            
            % vec_th_sq=f_sq_th.*(ones(length(vec_pred_sq),1)); % create a vector of TSD_threshold, so we do element-by-element binary operation
            % res_SQ =bsxfun(@ge,vec_th_sq,vec_pred_sq);  % If SQ is below th=0.85, the predic as poor SQ=1, and otherwise
            
            
            portion=f_sq_rat;
            
            % bb=(vec_pred_sq-min(vec_pred_sq)).*1/(max(vec_pred_sq)-min(vec_pred_sq)); % Normalize
            
            res_SQ = nan(length (vec_pred_sq),1);
            res_SQ (vec_pred_sq <= portion(1) ) = 0;
            res_SQ (vec_pred_sq >= portion(1) & vec_pred_sq <= portion(1)+ portion(2)) = 1;
            res_SQ (vec_pred_sq >  portion(1)+ portion(2) )=2;
            
            % compare=[ vec_pred_sq,res_SQ];
            
            
        end
        
        function [predicted_sbn]= BayesNetwrkMultilevel (cons,prdctn,PsbleKdePair,kdePredction)
            
            
            % sbs_th=size(PsbleKdePair,1);
            % % if sbs_th>2              % if large, divide in several batches
            % divsx=round(sbs_th/2);
            % endColx=sbs_th;
            %
            % rangex=single([1,divsx-1;divsx,endColx-1;endColx,endColx]);
            
            
            
            densityKDE_prob=cell(1,size(PsbleKdePair,1));
            for nxn=1:size(PsbleKdePair,1)
                bbbbb=PsbleKdePair(nxn,:);
                densityKDE_prob{nxn}=([bbbbb(kdePredction');bbbbb(kdePredction'+3);bbbbb(kdePredction'+6)])';
            end
            densityKDE_prob = vertcat (densityKDE_prob{:});
            
            % load ("dev_BN_Multilevel")
            
            newCombine=1+[prdctn.PRM_valid,prdctn.SQ_valid];
            
            
            AX =reshape( [1:1:9],3,3);
            AX(:,:,2) = reshape( [10:1:18],3,3);
            AX(:,:,3) = reshape( [19:1:27],3,3);
            
            % A=newCombine(:,1);
            % B=newCombine(:,2);
            c_a=1;
            p_cpt_con_rp=ones(size(newCombine,1),1);
            for bb=1:3
                
                bbB= bb*ones(size(newCombine,1),1);
                E=arrayfun(@(a,b,c)AX(a,b,c),newCombine(:,1),newCombine(:,2),bbB);
                p_cpt_con_rp(:,c_a)=cons(E,4);
                c_a=c_a+1;
                
                
                
            end
            repeatationTime=size(densityKDE_prob,1)/size(p_cpt_con_rp,1);
            p_cpt_con_rp = repmat(p_cpt_con_rp,repeatationTime,1);
            
            
            % p_cpt_obs_tr=densityKDE_prob; %% DONT DELETE
            
            BayesT_ProbVal = (densityKDE_prob.* p_cpt_con_rp)./...
                ((densityKDE_prob.* p_cpt_con_rp)+...
                ((1-densityKDE_prob).*(1-p_cpt_con_rp)));
            
            [~,predicted_sbn]=max([BayesT_ProbVal(:,1),BayesT_ProbVal(:,2),BayesT_ProbVal(:,3)],[],2);
            % predicted_sbn=[BayesT_ProbVal,i];
        end

    end
end
