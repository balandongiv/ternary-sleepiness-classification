
function [ev] =init_SBN (~)


load('NewDataSet_WithPVT_multilevel_021118.mat');
% load ('sleep_parameters_01Nov2017.mat');
load('Predicted_From_PRM_CR_fromExtendedData'); %Remember the result in this excel file only cover BL,SS1,..SS8

% Enable this to experiment if CR is phase lock>
%%% There are 3 lines need to be decomment when evaluating phase locking
%%% 2 line in ini_SBN and another in main_SbnKevinMurphy under the PRM
%%% evaluation
% load('CR_Manual'); % Enable this to experiment if CR is phase lock
ev.trialNo=table2array(at.sub (:,5)); % Extract Trial sequence (Column 5) at every wake bout


ev.table.pvt=at.result.PVTf10_All_SDay; 
ev.table.prm=Predicted_From_PRM_CR_fromExtendedData(:,8); %#ok<NODEF>
% ev.table.prm=Predicted_CR_Manual (:,8); %#ok<NODEF>  % Enable this to experiment if CR is phase lock
ev.table.sq_RAW=table2array (at.sub(:,[18:21,16]));
ev.state.kss=at.sub.KSS;

ev.cnd.SD  = {'BL' 'SS1' 'SS2' 'SS3' 'SS4' 'SS5' 'SS6' 'SS7' 'SS8'  'PS' 'R1' 'R2'};
% ev.cnd.SD  = {'BL' 'SS1' 'SS2'};


%%%% CHANGE THE TABLE LOCATION AND THE NAME IN THE MAIN FUNCTION
ev.Table_location=[at.table.sixEarly;at.table.eight;at.table.sixLate];
% ev.Table_location=[at.table.sixEarly];

%% since this switch have problem if i want to remove more than 1 subjct, need to disable it
% switch remSbj
%     case 'default'
        
%     otherwise
%          ev.Table_location(remSbj,:)=[];
% end

% ev.Table_location=[at.table.eight(1:2,:);at.table.sixLate(1:2,:)];



ev.cnd.Sbj= ev.Table_location.Properties.RowNames;  % FOR TESTING WHETHER IT CAN PROCESS ALL SHIFT DAY 
ev.Nsubjects=1:length (ev.cnd.Sbj);   % give every subjct the chance to become as a validation dataset

ev.FLoop.KdeLr_eta=1.00; %Default: 1.08: 1:0.01:1.4;
% ev.FLoop.prm_th=1: 0.01: 2.5; %ev.FLoop.prm_th evaluate this range: 1:0.01:5;
ev.FLoop.prm_th=2.18; %Default: 2.18>>ev.FLoop.prm_th evaluate this range: 1:0.01:5;
ev.FLoop.lap_th= 5;  %ev.FLoop.lap_th  evaluate this range 1:1:19
ev.FLoop.sq_th=0.85;


ev.ppsPair.val.lap_th=ev.FLoop.lap_th;
ev.ppsPair.val.KdeLr_eta=ev.FLoop.KdeLr_eta;
ev.ppsPair.val.prm_th=ev.FLoop.prm_th;
ev.ppsPair.val.sq_th=ev.FLoop.sq_th;
ev.ppsPair.val.total_sbj=ev.Nsubjects;
%%%%%%%%%%%%%

z.a=0.1:0.1:1;
z.b=0.1:0.1:1;
z.c=0.1:0.1:1;

[RangeFP, ~]=CmmProc.forloop2ndgrid(z);

RangeFP(:,1+end) = sum(RangeFP,2);
RangeFP=RangeFP(any(RangeFP(:,end)==1,2),:);

% ev.pair.Sq=[0.1,0.4];
% % 
% ev.pair.prm=[0.1,0.4];
ev.pair.Sq=single(RangeFP(:,1:2));
ev.pair.prm=single(RangeFP(:,1:2));
%%%%%%%%%%%%%%%%%%%%%

ev.ppsPair.rat.total_sbj=single(ev.Nsubjects);
ev.ppsPair.rat.pvt=single(1:1: size (ev.table.pvt,2));
ev.ppsPair.rat.prm_th=single(1: size (ev.pair.prm,1));
ev.ppsPair.rat.Sq=single(1: size (ev.pair.Sq,1));


%%%%%%%%%%%
%             ev.KDE.a=0.05:0.05:0.95;
%             ev.KDE.b=0.05:0.05:0.95;
%             ev.KDE.c=0.05:0.05:0.95;
%             
            ev.KDE.a=0.94;
            ev.KDE.b=0.05;
            ev.KDE.c=0.01;
            


[ev.position.lap_th,ev.position.KdeLr_eta, ev.position.prm_th,ev.position.sq_th,ev.position.total_sbj]=...
deal(1, 2, 3, 4, 5); % better assign awal2 macam ni, spaya x confuse downstream as, who column correspun what


[ev.position.Sen,ev.position.Spe,ev.position.SnSp,ev.position.Acc,...
 ev.position.Fmeasure,ev.position.Pre,ev.position.Rec]=deal(1,2,3,4,5,6,7); % better assign awal2 macam ni, spaya x confuse downstream as, who column correspun what

[ev.position.Kde0,ev.position.kde1,ev.position.PrmSq00,ev.position.PrmSq01,...
 ev.position.PrmSq10,ev.position.PrmSq11]=deal(1,2,3,4,5,6);


end

