% {'01MA';'02DF';'03AJ';'04LY';'05NS';'06TS';'07PL';'08GB';'09CT';'10ML';'11NB';'12SA';'13JH';'14MR';'15QQ';'16SB';'17JY';'18CG';'19RG';'20MH';'21JF';'22CF';'23LL';'24MS'}
% remSbj={'11NB','12SA'}; %
% remSbj={'12SA'}; %

tic
remSbj='default';

BaseName='TestBaseName';
base_path= pwd;
subfolder = 'MatFolders';

[ev] =init_SBN (remSbj);

ev.state.sq_percent=SubTech.SleepQuality(ev.table.sq_RAW);
ev=CmmProc.DatasetDivision (ev);

[PsbleKdePair]=SubTech.kde_prb(ev);
% PsbleKdePair(2,:)=1:9;

[sbj_ppsPair, ~]=CmmProc.forloop2ndgrid(ev.ppsPair.val);

[multilevelPair, ~]=CmmProc.forloop2ndgrid(ev.ppsPair.rat);

c_sbj=1; CaseTrial=1;


% for f_pair=1:size (multilevelPair,1)
for f_pair=1:240
    %
    f_sbj=multilevelPair(f_pair,1); f_lap_th=multilevelPair(f_pair,2);
    f_prm_th=multilevelPair(f_pair,3); f_sq_th=multilevelPair(f_pair,4);
    
    
    if f_sbj== c_sbj % Create the desired Cell
        AppendList=cell(24,3);
    else
    end
    
    [Result, repeatationTime]=EvalEachpair(ev,f_sbj,f_lap_th,f_prm_th,f_sq_th,PsbleKdePair);
    pairKde=multilevelPair(f_pair,2:end);
    if f_sbj<=23
        
        AppendList{f_sbj,1}=Result; AppendList{f_sbj,2}=repeatationTime;
        AppendList{f_sbj,3}=pairKde;
    elseif f_sbj==24
        AppendList{f_sbj,1}=Result; AppendList{f_sbj,2}=repeatationTime;
        AppendList{f_sbj,3}=pairKde;
        pairKde=multilevelPair(f_pair,2:end);
        FileMatName=CmmProc.getNames_Type1(CaseTrial,BaseName);
        save( fullfile( base_path, subfolder, FileMatName), 'AppendList');
        CaseTrial=CaseTrial+1;
    else
    end
    
    
end

totalTime=toc; % in seconds


function [Result, repeatationTime]=EvalEachpair (ev,f_sbj,f_lap_th,f_prm_th,f_sq_th,PsbleKdePair)


TrainSet_loc =single(cell2mat(ev.dataset.TrainingSet_cell(f_sbj)));
Validtn_Loc=single(cell2mat(ev.dataset.ValidationSet_cell(f_sbj)));

DataSet_PVT =ev.state.pvt(:,f_lap_th);
ev.PVT_train= DataSet_PVT(TrainSet_loc);
ev.PVT_valid= DataSet_PVT(Validtn_Loc);

ev.KSS_train= ev.state.kss(TrainSet_loc);
ev.KSS_valid= ev.state.kss(Validtn_Loc);


kdePredction=SubTech.EvalKDE (ev.KSS_valid,ev);
res_prm=SubTech.EvalPrm_multilevel(ev.state.prm,ev.pair.prm(f_prm_th,:)); % Enable this if normal experiment (i.e. not phase lock the CR)
state_sq=SubTech.EvalSQ_multilevel(ev.state.sq_percent,ev.pair.Sq(f_sq_th,:));

ev.prdctn.PRM_valid=res_prm(Validtn_Loc);
ev.prdctn.PRM_trainingReslt=res_prm(TrainSet_loc);

ev.prdctn.SQ_valid=state_sq(Validtn_Loc);
ev.prdctn.SQ_TrainingReslt=state_sq(TrainSet_loc);


ev.constantBN=SubTech.Murphy_tb(ev.prdctn.PRM_trainingReslt,ev.prdctn.SQ_TrainingReslt,ev.PVT_train);
[predicted_sbn_noisySignal]= SubTech.BayesNetwrkMultilevel(ev.constantBN,ev.prdctn,PsbleKdePair,kdePredction);

repeatationTime=size(predicted_sbn_noisySignal,1)/size(ev.PVT_valid,1);

Result=[repmat(ev.PVT_valid,repeatationTime,1),single(predicted_sbn_noisySignal)];

end

