function [total_sleep_drive,wake_effort]              ...
            = algebraic_dynamicVariable(time_out,model_out,s)
% time_hour 
VLPO                = model_out (:,1);
MA                  = model_out (:,2);
Homeo               = model_out (:,3);
Main_circa          = model_out (:,4);
Comple_circa        = model_out (:,4);


%% Calculate PRM  
% %% (3) algebraic calculation
Qv          = s.circ.Q_max./ (1.00000+ exp( -( VLPO - s.circ.theta)./s.circ.sigma));   % mean firing rate VLPO| Equation 1
% Qm          = s.circ.Q_max./ (1.00000+ exp( -( MA - s.circ.theta)./s.circ.sigma));    

%% Calculate circadian oscilator

C_old                   = (1+Main_circa).*0.5;
C_adj                   = ((3.1.*Main_circa) - (2.5.*Comple_circa) + 4.2)...
                            ./ (3.7.*(Main_circa + 2));
circadian               = 0.1 .* C_old + C_adj.^2;

% Find the minimum circadian
n =1440;

[mx,ix]=max(reshape([(circadian)' nan(1,mod(-length(circadian),n))],n,[]));

t_reshape   = reshape([(time_out)' nan(1,mod(-length(circadian),n))],n,[]); 
tmx         = zeros(1,length(mx));  

for day     = 1:length(ix),tmx(day) = t_reshape(ix(day),day);end




%% Core body temperature
%%% 3492s =0.97 h
core_body_temperature = [(tmx+3492); mx; 1:length(ix)];
disp('Just wonder why the CBT move rapidly eventhough during normal sleep condition')
datestr((core_body_temperature (1,:))./86400, 'HH:MM:SS.FFF')
% (core_body_temperature (1,:))
% cbt_verification = (mod((core_body_temperature (1,:)),86400))/3600
% core_body_temperature   = 0.97 + ...
%                         (-2.98.* (atan (model_out (:,4)./model_out (:,5))));            %S14, Postnova,2014


%% Calculate Total Sleep drive  
total_sleep_drive       = (s.con_strength.H_VLPO.*Homeo)...
                            + (s.con_strength.C_VLPO.*circadian) + s.pars.A_VLPO;  


wake_effort_unfiltered  = s.shift.vm_wake- s.pars.V_mv*Qv - s.pars.Am ;    
disp('Option for figure 6. Select 1 to filter only WEFFORT during F.Wakefulness OR 0 WEFFORT thru out the experiment')
% n = input('Enter a number: ');
% n=-1
n=1;
switch n
    case 1
        disp('negative one')
%% Set W.effort to zero when outside of F.wakefulness, since no additional drive required during this particular time.multiply by zero
% Set W.effort to its calculated value when under F.wakefulness. multiply by 1

time_zero_one            = mod((sum ((bsxfun(@lt,time_out,s.waketimeseconds)),2)),2);
wake_effort_filtered     = time_zero_one.*wake_effort_unfiltered ;
wake_effort_max_zero_one = bsxfun(@lt,0,wake_effort_filtered);
wake_effort              = wake_effort_max_zero_one.*wake_effort_filtered;
    case 0
%% Remove WEFFORT greater than 2
time_zero_one           = mod((sum ((bsxfun(@gt,2,wake_effort_unfiltered)),2)),2);
wake_effort     = time_zero_one.*wake_effort_unfiltered ;
   
    otherwise
        disp('Restart and select either 0 OR 1')
end


end
