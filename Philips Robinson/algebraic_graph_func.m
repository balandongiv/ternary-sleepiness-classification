function [total_sleep_drive,wake_effort, circadian, core_body_temperature]              ...
            = algebraic_graph_func(time_out,model_out,s)
% time_hour 
VLPO                = model_out (:,1);
MA                  = model_out (:,2);
Homeo               = model_out (:,3);
Main_circa          = model_out (:,4);
Comple_circa        = model_out (:,4);


%% Calculate PRM  
% %% (3) algebraic calculation
Qv          = s.circ.Q_max./ (1.00000+ exp( -( VLPO - s.circ.theta)./s.circ.sigma));   % mean firing rate VLPO| Equation 1
% Qm          = s.circ.Q_max./ (1.00000+ exp( -( MA - s.circ.theta)./s.circ.sigma));    

%% Calculate circadian oscilator

C_old                   = (1+Main_circa).*0.5;
C_adj                   = ((3.1.*Main_circa) - (2.5.*Comple_circa) + 4.2)...
                            ./ (3.7.*(Main_circa + 2));
circadian               = 0.1 .* C_old + C_adj.^2;

% Find the minimum circadian
n =1440;

[mx,ix]=max(reshape([(circadian)' nan(1,mod(-length(circadian),n))],n,[]));

t_reshape   = reshape([(time_out)' nan(1,mod(-length(circadian),n))],n,[]); 
tmx         = zeros(1,length(mx));  

for day     = 1:length(ix),tmx(day) = t_reshape(ix(day),day);end




%% Core body temperature
%%% 3492s =0.97 h
core_body_temperature = [(tmx+3492); mx; 1:length(ix)];
disp('Just wonder why the CBT move rapidly eventhough during normal sleep condition')
datestr((core_body_temperature (1,:))./86400, 'HH:MM:SS.FFF')
% (core_body_temperature (1,:))
% cbt_verification = (mod((core_body_temperature (1,:)),86400))/3600
% core_body_temperature   = 0.97 + ...
%                         (-2.98.* (atan (model_out (:,4)./model_out (:,5))));            %S14, Postnova,2014


%% Calculate Total Sleep drive  
total_sleep_drive       = (s.con_strength.H_VLPO.*Homeo)...
                            + (s.con_strength.C_VLPO.*circadian) + s.pars.A_VLPO;  


wake_effort_unfiltered  = s.shift.vm_wake- s.pars.V_mv*Qv - s.pars.Am ;    
disp('Option for figure 6. Select 1 to filter only WEFFORT during F.Wakefulness OR 0 WEFFORT thru out the experiment')
% n = input('Enter a number: ');
% n=-1
n=1;
switch n
    case 1
        disp('negative one')
%% Set W.effort to zero when outside of F.wakefulness, since no additional drive required during this particular time.multiply by zero
% Set W.effort to its calculated value when under F.wakefulness. multiply by 1

time_zero_one            = mod((sum ((bsxfun(@lt,time_out,s.WakeTimeSec )),2)),2);
wake_effort_filtered     = time_zero_one.*wake_effort_unfiltered ;
wake_effort_max_zero_one = bsxfun(@lt,0,wake_effort_filtered);
wake_effort              = wake_effort_max_zero_one.*wake_effort_filtered;
    case 0
%% Remove WEFFORT greater than 2
time_zero_one           = mod((sum ((bsxfun(@gt,2,wake_effort_unfiltered)),2)),2);
wake_effort     = time_zero_one.*wake_effort_unfiltered ;
   
    otherwise
        disp('Restart and select either 0 OR 1')
end

%% Just take w.effort that greater than zero. When Vm > W, then W is negetive, we set W=0, bcos no additional drive required to keep the system in wake state.


time_hour               = time_out /s.sph;
%% Plot model output
%%%%%%%%% figure (1) %%%%%%%%%%%%%%%%%%%%%
figure (1)
figure_1 = figure (1);
set(figure_1,'name','PRM','numbertitle','off')
plotmultiple= [(model_out (:,1)) (model_out (:,2)) (model_out (:,3)) ...
                        circadian total_sleep_drive wake_effort ];

ha = tight_subplot(6,1,.02,.1,.09);
 label_yaxis = {'Vv [mV]','Vm [mV]','H [mV]','Circadian', 'Total SDrive [mV]','WEffort [mV]'};
for i =1:6;
    
    axes(ha(i)); 
    plot    (time_out , plotmultiple (:,i), 'b');
    ylabel  (label_yaxis(i));
   
    
    if i <= 5; set(gca,'Xtick',[]); else end 
    if i == 6;
        axis([0 200 0 1.5])
%       max_a =(max(plotmultiple (:,i)))*1.1;
%       min_a =(max(plotmultiple (:,i)))*0.2; 
%       axis([0 100 min_a max_a])
    else
        
    end

end

% 
% figure_4 = figure (4);
% set(figure_4,'name','Core body temperature','numbertitle','off')
% set(gcf,'Position',[4,40,550,200]);
% plot    (time_hour , core_body_temperature, 'b');
% xlabel  ('t [hours]');
% ylabel  ('Vv [mV]');

% figure (5)
% plot (total_sleep_drive,wake_effort);
% 
figure (6)
time_hour               = time_out /s.sph;


plot (time_hour,wake_effort);
% axis([0 240 0 1.5])
ax(1)=gca;
set(ax(1),'Position',[0.12 0.12 0.75 0.70])
set(ax(1),'XColor','k','YColor','k');
set(ax(1),'Xtick',0:10:(s.days*s.hpd))

set(ax(1),'box','off')
ax(2)=axes('Position',get(ax(1),'Position'),...
   'XAxisLocation','top',...
   'Color','none',...
   'XColor','k','YColor','none');
b= 0;
% The top axis seem not stable, like chipsmore, sometime either using 6 or 6/240
% work
% set(ax(2),'Xtick',0:6:240)
set(ax(2),'Xtick',0:6/240:240)
for i = 1:6:240,
    a = mod(i,24);
    b=b+1;
    labeltimes_x (b)={a-1};

end

set(ax(2),'xticklabel',labeltimes_x)


xlabels{1} = 'Elapsed Time of Experiment (H)';
xlabels{2} = 'Time of the day (H)';
ylabels{1} = 'WEFFORT(mv)';


set(get(ax(1),'xlabel'),'string',xlabels{1})
set(get(ax(2),'xlabel'),'string',xlabels{2})
set(get(ax(1),'ylabel'),'string',ylabels{1})
% set


figure (7)


time=(0:s.eval.interval: (s.eval.end -1))';

% sleeptime_time_binary       = mod((sum ((bsxfun(@lt,time,s.SleepTimeSec)),2)),2);
% force_wakfulnes_time_binary = mod((sum ((bsxfun(@lt,time,s.WakeTimeSec)),2)),2);
% commuting_time_binary       = mod((sum ((bsxfun(@lt,time,s.CommutTimeSec)),2)),2);
% 
% light = (sleeptime_time_binary          == 1).* s.lightProf.LightOff +              ...
%     (commuting_time_binary          == 1).*s.lightProf.outdoor   +              ...
%     (force_wakfulnes_time_binary    == 1).*s.lightProf.indoor ; 

[light]  = lightProfile(time,s);

    
% Reshape from a colum vector into m by n matrix.
% The (86400/s.eval.interval) is required to balance the matrix, remember that
% the integration derivative is in 60 sec interval.
B = (reshape((light(:,1)),(86400/s.eval.interval),[])).';
subplot (2,1,1);
imagesc(B)
hold on;

% mod 86400 s =24h to convert extended duration into day per cycle
% divide by 60 to convert into 24h system since the x-axis imagesc in hour

plot (((mod((core_body_temperature (1,:)),86400))./s.eval.interval),...
        (core_body_temperature (3,:)),'rd','MarkerSize',8);
   
for i=1:11,CBT_time(i,:) = datestr((core_body_temperature (1,i))./86400, ...
        'HH:MM:SS.FFF');end

aa =(((mod((core_body_temperature (1,:)),86400))./60)+30);
bb = 1:11;

for i =1:10,  
text(aa (i),bb(i),CBT_time (i,:),'HorizontalAlignment','left') 
end

lapse =1;
set(gca,'YTick', 1:1:s.days );
set(gca,'XTick', 0:(((length (B))/24)*lapse):(length (B)));

for i = 1:24,labeltimes_x (i)={i-1};end

set(gca,'xticklabel',labeltimes_x)
xlabel ('time of day (h)')
ylabel ('day number')
grid on
set(gca, 'XColor', 'b')

% colorbar
colorbar('Ticks',[0,50, 100, 150,1000],...
         'TickLabels',{'0','50','100','150','1000'})
     
     
 %%%% FOR FUN PLOTING THE WEFFORT
%  figure (8)

 C = (reshape((wake_effort (1:14400,:)),(86400/s.eval.interval),[])).';
 
subplot (2,1,2);
imagesc(C)
lapse =1;
set(gca,'YTick', 1:1:s.days );
set(gca,'XTick', 0:(((length (B))/24)*lapse):(length (B)));

for i = 1:24,labeltimes_x (i)={i-1};end

set(gca,'xticklabel',labeltimes_x)
xlabel ('time of day (h)')
ylabel ('day number')
grid on
set(gca, 'XColor', 'b')
colorbar
end
