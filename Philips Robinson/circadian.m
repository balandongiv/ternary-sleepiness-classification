function [CIRCA_dt, COMPLE_CIRCA_dt, FRACT_PHOTO_RECPTR_dt, C_new]                  ...
            = circadian(time,Main_circa, Comple_circa,Frac_active_recep,s)  

rho         = 0.032;
r_tim       = 10 .* s.sph;
V_Xn        = rho;

[I]  = lightProfile(time,s);  
    
alpha = (s.circ.alpha_0 * ((I/s.circ.I0)^s.circ.power) * (I/(I+s.circ.I1)));


FRACT_PHOTO_RECPTR_dt   = alpha *(1-Frac_active_recep)                              ...         %  n_t
                        - (s.circ.beta * Frac_active_recep) ;   

%% Photic drive to the circadian oscillator
D_p                     = alpha *( 1-Frac_active_recep )                            ...
                        * ( 1 - (s.circ.epsilon * Main_circa))                      ...
                        * ( 1- (s.circ.epsilon * Comple_circa));                                 % Equation S12: Postnova,2016
%%                     
COMPLE_CIRCA_dt         = 1/ s.de.t_const.tau_ccirca *  ...                                      % Equation S11: Postnova,2016
                        ( ( D_p .*( (s.con_strength.ccirc_PDrive.* Comple_circa)    ...
                        -(s.con_strength.c_circa_PD .* Main_circa)))                ...
                        - (Main_circa.*(( s.circ.delta./ s.circ.tau_c ).^2 )) );                                

CIRCA_dt                = 1/ s.de.t_const.tau_circa  *(Comple_circa ... 
                        + (s.circ.gamma *( (1/3*Main_circa) + (4/3*Main_circa^3)    ...
                        - (256/105*Main_circa^7) )) ...
                        +  (s.con_strength.PDrive_circ* D_p) );                                
%% Calculate circadian
C_old                   = (1+Main_circa).*0.5;
C_adj                   = ((3.1.*Main_circa) - (2.5.*Comple_circa) + 4.2)...
                        ./ (3.7.*(Main_circa + 2));
C_new                   = 0.1 .* C_old + C_adj.^2;

end
