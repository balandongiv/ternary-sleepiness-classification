
s = struc_s(); 
initial_condition   = [ s.de.init_cond.VLPO s.de.init_cond.MA,              ...         % Initiate initial condition
                        s.de.init_cond.H s.de.init_cond.circ,               ...
                        s.de.init_cond.ccirc                                ...
                        s.de.init_cond.frac_photo_recptr];
            % (3) ODE Solver||
eval_time           = s.eval.start: s.eval.interval: s.eval.end; 
options             = odeset('RelTol', 1e-06, 'AbsTol', 1e-06);
j =1;
         
for chi_H =   [212400 312400]
    
    % s.chi_H = chi_H;
    % s.circa_q  =circa_q;
    % s.circa_k = circa_k;
    for circa_q = 1:2
        for circa_k =1:2
s.de.t_const.H                      = chi_H;           % chi 59,51,70 hours in Postnova,2016; Postnova,2013 and dot mat file,respectively
s.circa_q                           = circa_q;
s.circa_k                           = circa_k;
% s.con_strength.NPDrive_circ=;                         %rou : no unit 
s.circa_G                           = 37;

s.con_strength.PDrive_circ          = s.circa_G ;               %G: no unit
s.con_strength.ccirc_PDrive         = s.circa_q.*s.circa_G;     %qG: no unit V_YY
s.con_strength.c_circa_PD           = s.circa_k.*s.circa_G;     %kG: no unit V_YX 
            

 % Evaluate sleep-wake protocol for a xxx seconds for a xxx second interval
            
            
            
            
[time_out, model_out]       = ode15s (@model_evaluation, eval_time,                       ...
                initial_condition , options,s);
            
            
            %% Algebraic calculation & Produce graph
            
            
            [total_sleep_drive,wake_effort]   =       ...
                algebraic_dynamicVariable(time_out , model_out,s);
            b =1;
            
            %                     select only specific time stamp
            for z = [1 4 10]
                selectedTSD (b) = total_sleep_drive (z);
                b = b+1;
            end
            % Store into cell and carry forward
            s.index {j} = [chi_H circa_q circa_k selectedTSD];
            j = j+1;
            
        end
    end
end

c = [2.73829942567298,2.72889228375376,2.70993397367466]; % Mock number

for j = 1:2
s.index {j}(1,7) =  rms ((s.index {j}(1,4:6))-c);

end
% Note for future:  when comparing point by pint, use instant 


