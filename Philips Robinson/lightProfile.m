function [I]  = lightProfile(time,s)  

% This small function is neccesary since it will be called during @model_evaluation
% and @algebraic_graph_func routine | Since both function is using similar lines,
% better make a single function, plus can avoid mistake if I change the code at either
% one of the routine. Plus avoid need to change there and here as currently still in 
% calibrating process, with previous study

% Choose one of several cases, depending on which study author/year to
% evaluate
if s.case == 1
sleeptime_time_binary       = mod((sum ((bsxfun(@lt,time,s.SleepTimeSec)),2)),2);
force_wakfulnes_time_binary = mod((sum ((bsxfun(@lt,time,s.WakeTimeSec)),2)),2);
commuting_time_binary       = mod((sum ((bsxfun(@lt,time,s.CommutTimeSec)),2)),2);

I = (sleeptime_time_binary          == 1).* s.lightProf.LightOff +              ...
    (commuting_time_binary          == 1).*s.lightProf.outdoor   +              ...
    (force_wakfulnes_time_binary    == 1).*s.lightProf.indoor ; 



elseif s.case == 2   %% Postnova 2013
    
SleepTime_binary        = mod((sum ((bsxfun(@lt,time,s.SleepTimeSec)),2)),2);
IndoorTime_binary       = mod((sum ((bsxfun(@lt,time,s.IndoorTimeSec )),2)),2);
IndoorSummer_binary     = mod((sum ((bsxfun(@lt,time,s.IndoorSummerSec)),2)),2);
ConstantRoutine_binary  = mod((sum ((bsxfun(@lt,time,s.ConstantRoutineSec )),2)),2);
ShiftTime_binary        = mod((sum ((bsxfun(@lt,time,s.ShiftTimeSec  )),2)),2);
CommToWork_binary       = mod((sum ((bsxfun(@lt,time,s.CommToWorkSec )),2)),2);
CommToHome_binary       = mod((sum ((bsxfun(@lt,time,s.CommToHomeSec)),2)),2);


I = (SleepTime_binary         == 1).*s.lightProf.SleepTime          + ...
    (IndoorTime_binary        == 1).*s.lightProf.IndoorTime         + ...
    (IndoorSummer_binary      == 1).*s.lightProf.IndoorSummer       + ...
    (ConstantRoutine_binary   == 1).*s.lightProf.ConstantRoutine    + ...
    (ShiftTime_binary         == 1).*s.lightProf.ShiftTime          + ...
    (CommToWork_binary        == 1).*s.lightProf.CommToWork         + ...
    (CommToHome_binary        == 1).*s.lightProf.CommToHome ; 

end

end


