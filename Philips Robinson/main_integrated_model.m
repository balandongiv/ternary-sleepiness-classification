 
s = struc_short()   ; 

                                                          
initial_condition   = [ s.de.init_cond.VLPO s.de.init_cond.MA,         ...  % Initiate initial condition
                        s.de.init_cond.H s.de.init_cond.circ,          ...
                        s.de.init_cond.ccirc                           ...
                        s.de.init_cond.frac_photo_recptr];
% (3) ODE Solver|| 
eval_time           = s.eval.start: s.eval.interval: s.eval.end;            % Evaluate sleep-wake protocol for a xxx seconds for a xxx second interval

 
options             = odeset( 'RelTol', 1e-06, 'AbsTol', 1e-06);
        
[time_out, model_out]= ode15s (@model_evaluation, eval_time,                       ...
                               initial_condition , options,s);     
  

 %% Algebraic calculation & Produce graph

[total_sleep_drive,wake_effort, circadian, core_body_temperature]   =       ...
                        algebraic_graph_func(time_out , model_out,s);
time_hour               = time_out /s.sph;
%% Saving code to structure array
s.results.VLPO                      = model_out (:,1);
s.results.MA                        = model_out (:,2);
s.results.H                         = model_out (:,3);
s.results.main_circadian            = model_out (:,4);
s.results.complementary_circadian   = model_out (:,5);
s.results.fraction_photo_receptor   = model_out (:,6);
s.results.circadian                 = circadian;
s.results.total_sleep_drive         = total_sleep_drive;
s.results.wake_effort               = wake_effort;
s.results.core_body_temperature     = core_body_temperature;

mock_experimental_result.H          = model_out (:,3);
mock_experimental_result.time_out   = time_out;
mock_experimental_result.circadian  = circadian;
mock_experimental_result.WEFFORT    = wake_effort;




table_all=table(time_hour ,s.results.VLPO, s.results.MA, s.results.H, ...
                           s.results.circadian,s.results.total_sleep_drive );
filename = 'model_result_40to120TSD_200hour_.xlsx';

writetable(table_all,filename,'Sheet',1)

