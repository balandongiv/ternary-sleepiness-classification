% Please comment or decomment either one of the case.
% For normal modeling, decomment the " Case Model the system" (Line 8 & 9)
% For Case fitting, decomment the 'Case Fitting" (Line 16)

%% Case 1_Case Model the system
%%%%%% Start of Case 1_Case Model the system %%%%%%%%

% function RATES_dt = model_evaluation (time,RATES,s)
%  chi_H = s.de.t_const.H;

%%%%%% End of of Case 1_Case Model the system %%%%%%%%

%% Case 2_Case Fitting
%%%%%% Start of Case 2_Case Fitting %%%%%%%%

function RATES_dt = model_evaluation (time,RATES,s)

%%%%%% End of Case 2_Case Fitting %%%%%%%%

VLPO               = RATES(1);
MA                 = RATES(2);
Homeo              = RATES(3);
Main_circa         = RATES(4);
Comple_circa       = RATES(5);
Frac_active_recep  = RATES(6);
%% Call the circadian oscilator model from circadian.m function

[CIRCA_dt, COMPLE_CIRCA_dt, FRACT_PHOTO_RECPTR_dt, C_new] =                                     ...
                    circadian(time,Main_circa,Comple_circa,Frac_active_recep,s);

%% Calculate Total sleep drive
 
 D          = (s.con_strength.H_VLPO.*Homeo)...
            + (s.con_strength.C_VLPO.*C_new) + s.pars.A_VLPO;  
 
%% Algebraic calculation
Qv          = s.circ.Q_max./ (1.0 + exp( -( VLPO - s.circ.theta)./s.circ.sigma));            % mean firing rate VLPO| Equation 1
Qm          = s.circ.Q_max./ (1.0 + exp( -( MA - s.circ.theta)./s.circ.sigma));              % mean firing rate MA    Equation 3

%% Vv_t Calculation for the evolution of the mean potentiol of the VLPO population

 VLPO_dt    = (s.pars.V_vm .*Qm + D - VLPO)./ (s.de.t_const.VLPO);   
 
%% Calculation for the evolution of the mean potentiol of the MA population
wake        = mod(sum(s.WakeTimeSec  < time),2);

wake_effort = s.shift.vm_wake- s.pars.V_mv*Qv - s.pars.Am ;    
W_compare   = max ([0 wake_effort ]);

    % where Dm = Am; V_WE = minimum MA voltage during force wakefulness
W           = wake * W_compare;
% Notes: s.pars.Am = s.pars.Dm, Postnova,2016

MA_dt       = ((s.pars.V_mv.*Qv - MA + s.pars.Am  + W)...
              ./ (s.de.t_const.MA));                                                            % Equation 4

% wake_effort = forcedwake.wake_effort_minimum(s,wake,D_v,D_m,V(WEFFORT),V(lVLPO),V(lMA));      % calculate_weffort 
% lMA                = '-V(lMA) + s.pars.numv*sigmoid(V(lVLPO)) + D_m+ wake_effort';            % S2: Postnova,2016
% WEFFORT            = -V(WEFFORT) + wake_effort;                                             % WEFFORT 

%%
% H_t Calculate the  Homeostasis component
HOMEO_dt    =  ((s.con_strength.MA_H .*Qm - Homeo)./ s.de.t_const.H  );         


%% (6) Transfer temporal derivatives
RATES_dt    = [ VLPO_dt MA_dt  HOMEO_dt                         ...
              CIRCA_dt COMPLE_CIRCA_dt FRACT_PHOTO_RECPTR_dt]';
 
end
