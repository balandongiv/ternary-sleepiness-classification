function s = struc_s()
% Time conversion
s.sph                               = 3600;
s.hpd                               = 24;
s.mph                               = 60;
s.days                              = 10;
% evaluation time
% unit:Hours
s.eval.start                        = 0;                % Start time of the experimental protocol (seconds)
s.eval.end                          = s.days*s.hpd *s.sph;        % 9 days e.protocol including 2 day baseline and 2 days recovery: End time of the experimental protocol (seconds) (200 Hour)
s.eval.interval                     = 60;              % Interval of evaluation (seconds)| % Unit (Seconds) Sleep-wake transition occur in btw 2minute

% %% Initial Condition
% % PR-M
% % Unit:mV_miliVolt
s.de.init_cond.VLPO              = 2.3264;           % Initial condition of the VLPO
s.de.init_cond.MA                = -11.6522;         % Initial condition of the MA
s.de.init_cond.H                 = 13.2422;          % Baseline value of the Homeostasis
% 
% % St Hil
% 
s.de.init_cond.circ              = -0.17;                        
s.de.init_cond.ccirc             = -1.22;  
s.de.init_cond.frac_photo_recptr = 0.500;    
% 
% %% First order diffrentiation time constant
% 
% % PR-M
s.de.t_const.VLPO                = 10;               % tau_v
s.de.t_const.MA                  = 10;               % tau_m

omicron                          = 2* pi/(24* s.sph); % Normalize to second^-1
s.de.t_const.tau_circa           = 1/omicron;         % Time constant of the circadian variable
s.de.t_const.tau_ccirca          = 1/omicron;      % Time constant of the circadian variable_complementary circadian
% 
% % St Hil
% %% Connection strength
s.con_strength.C_VLPO               = -0.5;             % VvC   % Unit: mV_ Value validate. In postnova,2013 , the value  -5.8mV , Postnova 2016 - 0.5
s.con_strength.H_VLPO               = 1.0;              % VvH | mV  % V_vh  Unit: mV_ Value validate Time constant of the homeostatic drive
s.con_strength.MA_H                 = 4.40;             % Unit: - _ Value validate mu_H; Postnova,2016 they assign unit second,      




%  Dm = Am Postnova,2016
% s.pars.A_MA                          = ;                      % Combined external influences on the MA: mv
 s.pars.A_VLPO                         = -10.3;                 % unit: mV Constant input from other population going into VLPO: -10.3 and -11.6 from Postnova,2016 and Postnova,2013 respectively

 %% Connection strenght of X on Xc itself modulated by the photic drive Dp
s.pars.V_vm                             = -2.10;                % V_vm unit: mV s
s.pars.V_mv                             = -1.80;                % V_mv unit: mV s
s.pars.Am                               = 1.3;                  % A   unit:mV

s.circ.gamma                            = 0.13;
s.circ.delta                            = ( 24* s.sph)/0.99729;
s.circ.tau_c                            = 24.2*s.sph;
s.circ.beta                             = (0.007)/s.mph;        % In second
s.circ.alpha_0                          = 0.1/s.mph;
s.circ.epsilon                          = 0.4;                  % Dimensionless
s.circ.I0                               = 9500;
s.circ.I1                               = 100;
s.circ.power                            = 0.5;
s.circ.Q_max                            = 100;                                      % Unit: sec^-1_ Value validate
s.circ.theta                            = 10.0;                                     % Unit: mV_ Value validate
s.circ.sigma                            = 3.0;                                      % Unit: mV_ Value validate
s.shift.vm_wake                         = 0.07;                                     %In Postnova 2016 Table S2 = 0.07 in the old model versions Vm_wake_value = mean(Vm) during wake.
s.condition.circ.sleeptime              = [0 21600]; 

s.normal_sun_light_time   = ([9 18 33 42 57 66 81 90])*3600;
                        
s.sleeptime_time          = ([0 8 24 32 48 56 72 80])*3600; 
s.commuting_time          =([8 9 18 24 32 33 42 48 56 57 66 72 ...
                            80 81 90 96])*3600;
%%% Normal Sleep
% s.waketimehours = []; 
%%% Total Sleep Deprive
s.waketimehours = [96 192];    % TSD for four (4) days

%%%% Chronic Sleep
% s.waketimehours = [ 74.50 80.50 86.50 92.50 ... % Day 4_ Morning & Evening shift
%                     98.50 104.5 110.5 116.5 ... % Day 5_ Morning & Evening shift
%                     122.5 128.5 134.5 140.5 ... % Day 6_ Morning & Evening shift
%                     146.5 152.5 158.5 164.5];   % Day 7_ Morning & Evening shift
 

s.waketimeseconds = s.waketimehours * s.sph;

end