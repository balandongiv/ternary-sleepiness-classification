% Specifically for Short 2016
% Decomment for normaly computation
% function s = struc()
% Decomment for find fitting
function s = struc_short()

% Time conversion
s.sph                            = 3600;
s.hpd                            = 24;
s.mph                            = 60;
s.days                           = 25;
% evaluation time
% unit:Hours
s.eval.start                     = 0;                % Start time of the experimental protocol (seconds)
s.eval.end                       = s.days*s.hpd *s.sph;        % 9 days e.protocol including 2 day baseline and 2 days recovery: End time of the experimental protocol (seconds) (200 Hour)
s.eval.interval                  = 60;              % Interval of evaluation (seconds)| % Unit (Seconds) Sleep-wake transition occur in btw 2minute

% %% Initial Condition
% % PR-M
% % Unit:mV_miliVolt
s.de.init_cond.VLPO              = 2.3264;           % Initial condition of the VLPO
s.de.init_cond.MA                = -11.6522;         % Initial condition of the MA
s.de.init_cond.H                 = 13.2422;          % Baseline value of the Homeostasis
% 
% % St Hil
% 
s.de.init_cond.circ              = -0.17;                        
s.de.init_cond.ccirc             = -1.22;  
s.de.init_cond.frac_photo_recptr = 0.500;    
% 
% %% First order diffrentiation time constant
% 
% % PR-M
s.de.t_const.VLPO                = 10;                  % tau_v
s.de.t_const.MA                  = 10;                  % tau_m
s.de.t_const.H                   = 212400;              % chi 59,51,70 hours in Postnova,2016; Postnova,2013 and dot mat file,respectively
omicron                          = 2* pi/(24* s.sph);   % Normalize to second^-1
s.de.t_const.tau_circa           = 1/omicron;           % Time constant of the circadian variable
s.de.t_const.tau_ccirca          = 1/omicron;           % Time constant of the circadian variable_complementary circadian
% 
% % St Hil
% %% Connection strength
s.con_strength.C_VLPO               = -0.5;             % VvC   % Unit: mV_ Value validate. In postnova,2013 , the value  -5.8mV , Postnova 2016 - 0.5
s.con_strength.H_VLPO               = 1.0;              % VvH | mV  % V_vh  Unit: mV_ Value validate Time constant of the homeostatic drive
s.con_strength.MA_H                 = 4.40;             % Unit: - _ Value validate mu_H; Postnova,2016 they assign unit second,      

% s.con_strength.NPDrive_circ=;                         %rou : no unit 
s.circa_G                           = 37*60;
s.circa_q                           = 0.6;
s.circa_k                           = 0.55;
s.con_strength.PDrive_circ          = s.circa_G ;               %G: no unit
s.con_strength.ccirc_PDrive         = s.circa_q.*s.circa_G;     %qG: no unit V_YY
s.con_strength.c_circa_PD           = s.circa_k.*s.circa_G;     %kG: no unit V_YX

%  Dm = Am Postnova,2016
% s.pars.A_MA                          = ;                      % Combined external influences on the MA: mv
 s.pars.A_VLPO                         = -10.3;                 % unit: mV Constant input from other population going into VLPO: -10.3 and -11.6 from Postnova,2016 and Postnova,2013 respectively

 %% Connection strenght of X on Xc itself modulated by the photic drive Dp
s.pars.V_vm                             = -2.10;                % V_vm unit: mV s
s.pars.V_mv                             = -1.80;                % V_mv unit: mV s
s.pars.Am                               = 1.3;                  % A   unit:mV

s.circ.gamma                            = 0.13;
s.circ.delta                            = ( 24* s.sph)/0.99729;
s.circ.tau_c                            = 24.2*s.sph;
s.circ.beta                             = (0.007)/s.mph;        % In second
s.circ.alpha_0                          = 0.1/s.mph;
s.circ.epsilon                          = 0.4;                  % Dimensionless
s.circ.I0                               = 9500;
s.circ.I1                               = 100;
s.circ.power                            = 0.5;
s.circ.Q_max                            = 100;                  % Unit: sec^-1_ Value validate
s.circ.theta                            = 10.0;                 % Unit: mV_ Value validate
s.circ.sigma                            = 3.0;                  % Unit: mV_ Value validate
s.shift.vm_wake                         = 0.07;                 % In Postnova 2016 Table S2 = 0.07 in the old model versions Vm_wake_value = mean(Vm) during wake.
s.condition.circ.sleeptime              = [0 21600]; 

s.case = 1;
%% light profile
s.lightProf.LightOff    = 0.03;
s.lightProf.indoor      = 120;
s.lightProf.outdoor     = 500;
%% Shift day start from day 19 to 23

s.commuting_time          =    [8	8.5	    19.5	20  ... % Day 1
                                32	32.5	43.5	44  ... % Day 2
                                56	56.5	67.5	68  ... % Day 3
                                80	80.5	91.5	92  ... % Day 4
                                104	104.5	115.5	116 ... % Day 5
                                128	128.5	139.5	140 ... % Day 6
                                152	152.5	163.5	164 ... % Day 7
                                176	176.5	187.5	188 ... % Day 8
                                200	200.5	211.5	212 ... % Day 9
                                224	224.5	235.5	236 ... % Day 10
                                248	248.5	259.5	260 ... % Day 11
                                272	272.5	283.5	284 ... % Day 12
                                296	296.5	307.5	308 ... % Day 13
                                320	320.5	331.5	332 ... % Day 14
                                344	344.5	355.5	356 ... % Day 15
                                368	368.5	379.5	380 ... % Day 16
                                392	392.5               ];  % Day 17
               
s.sleeptime_time          =    [0	8	24	32          ... % Day 1  & 2
                                48	56	72	80          ... % Day 3  & 4
                                96	104	120	128         ... % Day 5  & 6
                                144	152	168	176         ... % Day 7  & 8
                                192	200	216	224         ... % Day 9  & 10
                                240	248	264	272         ... % Day 11 & 12
                                288	296	312	320         ... % Day 13 & 14
                                336	344	360	368         ... % Day 15 & 16
                                384	392	406	416         ... % Day 17 & 18
                                430	440	447	452         ... % Day 18 & 19
                                459	464	471	476         ... % Day 20
                                483	488	495	500         ... % Day 21
                                507 512 519 524         ... % Day 22
                                531	536 550 560         ... % Day 23 & 24
                                574	584	                ];  % Day 25



s.waketimehours =             [ 8.5	    19.5	20	 24  ... % Day 1
                                32.5	43.5	44	 48  ... % Day 2
                                56.5	67.5	68	 72  ... % Day 3
                                80.5	91.5	92	 96  ... % Day 4
                                104.5	115.5	116	 120 ... % Day 5
                                128.5	139.5	140	 144 ... % Day 6
                                152.5	163.5	164	 168 ... % Day 7
                                176.5	187.5	188	 192 ... % Day 8
                                200.5	211.5	212	 216 ... % Day 9
                                224.5	235.5	236	 240 ... % Day 10
                                248.5	259.5	260	 264 ... % Day 11
                                272.5	283.5	284	 288 ... % Day 12 
                                296.5	307.5	308	 312 ... % Day 13 
                                320.5	331.5	332	 336 ... % Day 14
                                344.5	355.5	356	 360 ... % Day 15
                                368.5	379.5	380	 384 ... % Day 16
                                392.5	403.5	404	 406 ... % Day 17
                                416     430     440  447 ... % Day 18 & 19
                                452     459     464	 471 ... % Day 19 & 20
                                476     483     488	 495 ... % Day 20 & 21
                                500     507     512	 519 ... % Day 21 & 22
                                524     531     536	 550 ... % Day 22 & 23
                                560     574     584	 600 ];  % Day 24 & 25
% c= 1;
%                             for i = 1:end
% sleeptime  (c,:)  = excel (:,i);
% sleeptime  (c+1,:) = excel (:,i+1);
% c =c+1
% end

s.SleepTimeSec  = s.sleeptime_time * s.sph;
s.CommutTimeSec = s.commuting_time * s.sph;
s.WakeTimeSec   = s.waketimehours * s.sph;

end
