function s = struc_sveta(CaseStudy)


% function b = ReadLightProfile (studycase);
% select case
% 2 = Postnova
% 1 = Postnova
% 3 = Default case
% studycase = 1



% To compensate for diffrent cases for diffrent studies (e.g., Postnova
% 2013 - Control & Treatment; Short 2016 - Shift Schedule; The 25h SD;
% normal sleep & awake pattern), this structure was divided into several
% if-else cases. Other parameters that fix/ not changed for the diffrent cases will be put
% outside the if-else container

s.eval.interval                  = 60;                      % Interval of evaluation (seconds)| % Unit (Seconds) Sleep-wake transition occur in btw 2minute

%% Fix Variables
% Time conversion
s.sph                            = 3600;
s.hpd                            = 24;
s.mph                            = 60;

% %% Initial Condition
% % PR-M
% % Unit:mV_miliVolt
s.de.init_cond.VLPO              = 2.3264;           % Initial condition of the VLPO
s.de.init_cond.MA                = -11.6522;         % Initial condition of the MA
s.de.init_cond.H                 = 13.2422;          % Baseline value of the Homeostasis
% 
% % St Hil
% 
s.de.init_cond.circ              = -0.17;                        
s.de.init_cond.ccirc             = -1.22;  
s.de.init_cond.frac_photo_recptr = 0.500;    
% 
% %% First order diffrentiation time constant
% 
% % PR-M
s.de.t_const.VLPO                = 10;                  % tau_v
s.de.t_const.MA                  = 10;                  % tau_m
s.de.t_const.H                   = 212400;              % chi 59,51,70 hours in Postnova,2016; Postnova,2013 and dot mat file,respectively
omicron                          = 2* pi/(24* s.sph);   % Normalize to second^-1
s.de.t_const.tau_circa           = 1/omicron;           % Time constant of the circadian variable
s.de.t_const.tau_ccirca          = 1/omicron;           % Time constant of the circadian variable_complementary circadian
% 
% % St Hil
% %% Connection strength
s.con_strength.C_VLPO               = -0.5;             % VvC   % Unit: mV_ Value validate. In postnova,2013 , the value  -5.8mV , Postnova 2016 - 0.5
s.con_strength.H_VLPO               = 1.0;              % VvH | mV  % V_vh  Unit: mV_ Value validate Time constant of the homeostatic drive
s.con_strength.MA_H                 = 4.40;             % Unit: - _ Value validate mu_H; Postnova,2016 they assign unit second,      

% s.con_strength.NPDrive_circ=;                         %rou : no unit 
s.circa_G                           = 37*60;
s.circa_q                           = 0.6;
s.circa_k                           = 0.55;
s.con_strength.PDrive_circ          = s.circa_G ;               %G: no unit
s.con_strength.ccirc_PDrive         = s.circa_q.*s.circa_G;     %qG: no unit V_YY
s.con_strength.c_circa_PD           = s.circa_k.*s.circa_G;     %kG: no unit V_YX

%  Dm = Am Postnova,2016
% s.pars.A_MA                          = ;                      % Combined external influences on the MA: mv
 s.pars.A_VLPO                         = -10.3;                 % unit: mV Constant input from other population going into VLPO: -10.3 and -11.6 from Postnova,2016 and Postnova,2013 respectively

 %% Connection strenght of X on Xc itself modulated by the photic drive Dp
s.pars.V_vm                             = -2.10;                % V_vm unit: mV s
s.pars.V_mv                             = -1.80;                % V_mv unit: mV s
s.pars.Am                               = 1.3;                  % A   unit:mV

s.circ.gamma                            = 0.13;
s.circ.delta                            = ( 24* s.sph)/0.99729;
s.circ.tau_c                            = 24.2*s.sph;
s.circ.beta                             = (0.007)/s.mph;        % In second
s.circ.alpha_0                          = 0.1/s.mph;
s.circ.epsilon                          = 0.4;                  % Dimensionless
s.circ.I0                               = 9500;
s.circ.I1                               = 100;
s.circ.power                            = 0.5;
s.circ.Q_max                            = 100;                  % Unit: sec^-1_ Value validate
s.circ.theta                            = 10.0;                 % Unit: mV_ Value validate
s.circ.sigma                            = 3.0;                  % Unit: mV_ Value validate
s.shift.vm_wake                         = 0.07;                 % In Postnova 2016 Table S2 = 0.07 in the old model versions Vm_wake_value = mean(Vm) during wake.


s.condition.circ.sleeptime              = [0 21600]; %%% Warning |untuk apa ni aa



%% Total study days which always start with 7 days to stabalize the ODE model, followed by 
%% normal sleep at house from day 8-16. The experimental always start from day 17 onwards

if CaseStudy == 1  % Postnova,2013| For control case
 s.days                           = 20;

else
  s.days                           = 20;
end


% evaluation time
% unit:Hours
s.eval.start                     = 0;                       % Start time of the experimental protocol (seconds)
s.eval.end                       = s.days*s.hpd *s.sph;     % 9 days e.protocol including 2 day baseline and 2 days recovery: End time of the experimental protocol (seconds) (200 Hour)


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Section: Light Profile
% Required since diff study and case have diff light profile
% Light Profle, unit in Lux

%% Start evaluate the light profile: Postnova,2013| For control case
if CaseStudy == 1   % Postnova,2013| For control case
s.lightProf.SleepTime           = 0;        
s.lightProf.IndoorTime          = 150;         
s.lightProf.IndoorSummer        = 500;    
s.lightProf.ConstantRoutine     = 150;
s.lightProf.ShiftTime           = 150;
s.lightProf.CommToWork          = 25;
s.lightProf.CommToHome          = 5000;

%% Start evaluate the light profile: Postnova,2013| For Treatment case
if s.case == 2   % Postnova,2013| For Treatment case
s.lightProf.SleepTime           = 0;        
s.lightProf.IndoorTime          = 150;         
s.lightProf.IndoorSummer        = 500;    
s.lightProf.ConstantRoutine     = 150;
s.lightProf.ShiftTime           = 150;
s.lightProf.CommToWork          = 25;
s.lightProf.CommToHome          = 5000;  
    
else
%% light profile
s.lightProf.LightOff    = 0.03;
s.lightProf.indoor      = 120;
s.lightProf.outdoor     = 500;

end

%% Start evaluate the time awake, sleep, shift: Postnova,2013| For Control case
%% Used diffrent function to handle exporting the hour2
CaseStudy == 1   % Postnova,2013| For control case
s.sleeptime_time          =   [ 0       8       ...
                                24      32      ...
                                48      56      ...
                                72      80      ...
                                96      104     ...
                                120     128     ...
                                144     152     ...
                                168     176     ...
                                192     200     ...
                                216     224     ...
                                240     248     ...
                                264     272     ...
                                288     296     ...
                                323	    325     ...
                                333.5   335     ...
                                345     349     ...
                                357.5	359     ...
                                369     373     ...
                                381.5	383     ...
                                393     396     ...
                                405.5	407     ...
                                417     320     ...
                                452     465.5   ...
                                477.5	480];


                         
                            
s.indoor_time          =   [8       9	 20     24  ...
                            32      33	 44     48  ...
                            56      57	 68     72  ...
                            80      81	 92     96  ...
                            104     105	 116	120 ...
                            128     129	 140	144 ...
                            152     153	 164	168 ...
                            176     177	 188	192 ...
                            200     201	 212	216 ...
                            224     225	 236	240 ...
                            248     249	 260	264 ...
                            272     273	 284	288 ...
                            332     333.5           ...	
                            356     357.5           ...
                            380     381.5           ...
                            404     405.5           ...
                            476     477.7	];	

s.indoorSummer_time  =     [9.5     20              ...
                            33.5	44              ...
                            57.5	68              ...
                            81.5	92              ...              
                            105.5	116             ...
                            129.5	140             ...
                            153.5	164             ...
                            177.5	188             ...
                            201.5	212             ...
                            225.5	236             ...
                            249.5	260             ...
                            273.5	284             ...
                            326     332             ...
                            349     356             ...
                            373     380             ...
                            396     404             ...
                            420     425             ...
                            465.5	476];

s.ConstantRoutine_time =   [296.5	323             ...
                            425     452];


s.shift_time           =   [336     344             ...
                            360     368             ...
                            384     392             ...
                            408     416] ;
                        
                        
s.CommToWork_time      = [  335     336             ...
                            359     360             ...
                            383     384             ...
                            407     408];    

 
s.CommToHome_time       = [ 344	345                 ...
                            368	369                 ...
                            392	393                 ...
                            416	417];

else
    
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
WakeTime = s.sleeptime_time (2:end);   % The waketime always occur inverse to the sleep time
                                       % Say sleep time between 0~8 &
                                       % 24~32, thus waketime should be
                                       % between 8~24; 32 to some hour
                                       
s.SleepTimeSec          = s.sleeptime_time * s.sph;
s.IndoorTimeSec         = s.indoor_time * s.sph;
s.IndoorSummerSec       = s.indoorSummer_time * s.sph;
s.ConstantRoutineSec    = s.ConstantRoutine_time * s.sph;
s.ShiftTimeSec          = s.shift_time * s.sph;
s.CommToWorkSec         = s.CommToWork_time * s.sph;
s.CommToHomeSec         = s.CommToHome_time * s.sph;
s.WakeTimeSec           = WakeTime* s.sph;

end
