% 
% Decomment for normaly computation
% function s = struc()
% Decomment for find fitting
function s = struc_v(Var_dynamic)


s.de.t_const.H                      = Var_dynamic.chi_H;           % chi 59,51,70 hours in Postnova,2016; Postnova,2013 and dot mat file,respectively
s.circa_q                           = Var_dynamic.circa_q;
s.circa_k                           = Var_dynamic.circa_k;
% s.con_strength.NPDrive_circ=;                         %rou : no unit 
s.circa_G                           = 37;

s.con_strength.PDrive_circ          = s.circa_G ;               %G: no unit
s.con_strength.ccirc_PDrive         = s.circa_q.*s.circa_G;     %qG: no unit V_YY
s.con_strength.c_circa_PD           = s.circa_k.*s.circa_G;     %kG: no unit V_YX

end
