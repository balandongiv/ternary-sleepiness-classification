# Introduction
This is the collection of code routine used in the thesis: "Maritime shift workers sleepiness detection system with multi-sleepiness indicator". The objective of this thesis are

1) To investigate the benefit of post-processing of subjective sleepiness estimation by using the Likelihood Ratio test and Kernel Density Estimation technique in reducing the effect of response biases.

2. To design a ternary SDS based on Bayesian Network for better classification ability, reliability, and capability in lengthening prediction horizon.

# Copyright
No rights are given to reproduce or modify this work.

# Principle
I believe that all academic research should be made more open. This is a small step towards such a philosophy. (Making my code itself available would be better again, but one step at a time.)

# Requirement 
Matlab (2017 and above)

# Paper Citation

If you use any of the model in your research and found it helpful, please cite the following paper:

`j@article{balandong2019, author={R. P. Balandong, T. T. Bong, M. Short, and M. N. Saad},
  title={Maritime Shift Workers Sleepiness Detection System With Multi-Modality Cues},
  journal={IEEE Access},
  volume={7},
  url={https://ieeexplore.ieee.org/abstract/document/8764453},
  year={2019}}`
