

fileName='Result6E8H6L_04Sept2018_KDE_DiffTh_T1';
DataAsInExcel=eval_DataAsInExcel(fileName);


function DataAsInExcel=eval_DataAsInExcel(fileName)
[s,result]=init_evl_Type1(fileName);
[~,newObservation_unfiltered]=reshape_eachSubfastVer(s,result);


AllTypeOfperf_allSbj_CELL=cell(size(newObservation_unfiltered,1),1);
for f_v=1:size(newObservation_unfiltered,1)
xx=newObservation_unfiltered(f_v,:);
yyy=(cellfun( @(x) x(1,:), xx,'Un',0 )); 
vv=vertcat (yyy{:,:});
AllTypeOfperf_allSbj_CELL{f_v,1}=[vv; (mean(vv,1))];
end

%%% Convert into table for easy viewing
varname={'sKss_No' 'Sen' 'Spe' 'SnsP' 'Acc' 'F1' 'Pre' 'rec'};
            
AllTypeOfperf_allSbj_Table = cellfun( @(m) array2table(m,'VariableNames',varname) ...
                            ,AllTypeOfperf_allSbj_CELL,'Un',0 );
                        
 DataAsInExcel=AllTypeOfperf_allSbj_Table {1,:};                       
                        
end




function [s,newObservation_unfiltered]=reshape_eachSubfastVer(s,result)


vec_sKss_table=cellfun(@(S) S.sKss, result, 'uni',true);
sKss_COMB=ConvertNSeqNo(vec_sKss_table,0.00); % Result for all subjects which cover for X pvt,prm,sq combination. In this, for diff pvt_th


newObservation_unfiltered = arrayfun(@(k) sKss_COMB(k,:),1:s.TotalCombntn,'un',0);


newObservation_unfiltered =reshape(newObservation_unfiltered,[s.nRows_EachSbj,s.Total_Subjct]);

end


function feature_vec_Convertd=Calculate_SnSp(s,feature_table,SeqNo)
vec_feature = reshape(struct2array(feature_table), [], numel(feature_table)).';
vec_feature(:,5)=(vec_feature(:,s.position.Sn)+vec_feature(:,s.position.Sp))./2;
feature_vec_Convertd=[(repmat (SeqNo,numel(feature_table),1)),vec_feature];
end
    
    
function [s,resultS]=init_evl(fileName)
fr=load(fileName); % Light

resultS=fr.result;
s.position=fr.Var_position;


s.Total_Subjct=max(fr.sbj_ppsPair(:,s.position.total_sbj+1)); % ? Shift One,sbb kde_lr_eta tu kena letak d column 2

s.nRows_EachSbj=sum ((fr.sbj_ppsPair(:,s.position.total_sbj+1)==1)==1); % To find, each subject used how many rows. For ex, subjct 1 have hw many rows
s.TotalCombntn=s.Total_Subjct*s.nRows_EachSbj;
% %% Pairs of PVT_PRM Seq No,pvt_th and prm_th,Sq_Th
s.Pvt_Prm_Sq=[(1:s.nRows_EachSbj)', fr.sbj_ppsPair((1:s.nRows_EachSbj)',1:3)];

s.nfeature=1; % It is not a feature in this case, instead only thKSS
end
