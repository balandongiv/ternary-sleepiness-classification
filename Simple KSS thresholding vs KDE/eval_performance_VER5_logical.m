%%% 20 December 2017: 
%%% This file was used to evaluate how our system able to
%%% mitigate if the subjective sleepiness information is not available. In
%%% this project, the SBN assume the state of the KSS is equivalent to the 
%%% state of PRM at that particular moment, whenever if no KSS info is 
%%% available. Whereas, in typical KDE, if no kss is available, kde always 
%%% assume that it is sleepy at that particular moment. 

%%% The Pvt_th, Prm_th, sq_th was 6, 1.85, and 0.85, respectively. 
%%% The work shift protocol was 8h and 6 Late groups.

% This version is much faster than the previous version, specifically if we want
% to evaluate diffrent sets of free parameters. The sample is evaluated in 
% a vector form.
%% The performance has been validated, by running line by line and compare
% with manual calculation in the excel software (Check 23 Nov 2017)
function [sensitivity,specificity,accuracy,f_measure]=eval_performance_VER5_logical(True_Label,Predicted_Label)
% load ('dev_nan');
nDim=size(Predicted_Label,2);
True_Label=repmat(True_Label,[nDim 1]);
Predicted_Label=Predicted_Label(:);
%% TN=1, FN=2, FP=3, TP=4

ConfMat = single(ones(length(True_Label), 1));       % True Negative  (TN)  | Predicted Alert(False), actual Alert(False)
ConfMat (logical (((Predicted_Label==0) .*(True_Label==1))))=2; % False Negative (FN)  | Predicted Alert(False), Sleepy (True)
ConfMat (logical (((Predicted_Label==1) .*(True_Label==0))))=3; % False Positive (FP)  | Predicted Sleepy (True), actual Alert(False)
ConfMat (logical (((Predicted_Label==1) .*(True_Label==1))))=4; % True Positive (TP)   | Predicted Sleepy (True), actual Sleepy (True)



ConfMat=reshape(ConfMat,[],nDim);

sum_TP=sum((ConfMat == 4),1);
sum_TN=sum((ConfMat == 1),1);
sum_FN=sum((ConfMat == 2),1);
sum_FP=sum((ConfMat == 3),1);

%% This section calculate in a vector form
sensitivity  = (single(sum_TP./(sum_TP+ sum_FN))).*100;   
specificity  = (single(sum_TN./(sum_FP+ sum_TN))).*100;   

accuracy   = (single((sum_TP +sum_TN)./(sum_TP + sum_TN + sum_FN +sum_FP))).*100;

%% Recall and precision, see Inductive Logic Programming, for reference
%% OR Foundations of Statistical Natural Language Processing> See page 269
precision= (single(sum_TP./(sum_TP+sum_FP))).*100;
precision(isnan(precision))=0;

% recall = sensitivity;
% f_measure = 2*((precision*recall)/(precision + recall));

beta=1; % used the youtube version and give more weight to precision

beta2=single(1); % used the typical wikipidia version and put more weight to precision
f_measure=((beta2+1).*(precision.* sensitivity))./((beta2.*precision)+sensitivity); % Typical equation, beta 0.5 emphasize precision, beta 2 emphasize recal, beta 1, equal weight to recal and precision

%% as in the youtube & Foundations of Statistical Natural Language Processing> See page 269
% f_measure=1./((beta./precision)+((1-beta)./sensitivity)); % beta=0.5 give equal weight to precision and recall, beta=1 more weight to precision

% f_measure = 2.*((precision.* sensitivity)./(precision +  sensitivity)); % In this form, beta is 1, mean, we put more weight into precision



f_measure(isnan(f_measure))=0;
end