function [ev] =init_SBN (remSbj)
load('NewDataSet_WithPVT_lapseNf10_29Oct2017.mat');
% load ('sleep_parameters_01Nov2017.mat');
load('Predicted_From_PRM_CR_fromExtendedData'); %Remember the result in this excel file only cover BL,SS1,..SS8
ev.trialNo=table2array(at.sub (:,5)); % Extract Trial sequence (Column 5) at every wake bout


ev.table.pvt=at.result.PVTf10_All_SDay; 
ev.table.prm=Predicted_From_PRM_CR_fromExtendedData(:,8); %#ok<NODEF>
ev.table.sq_RAW=table2array (at.sub(:,[18:21,16]));
ev.state.kss=at.sub.KSS;

ev.cnd.SD  = {'BL' 'SS1' 'SS2' 'SS3' 'SS4' 'SS5' 'SS6' 'SS7' 'SS8'  'PS' 'R1' 'R2'};
% ev.cnd.SD  = {'BL' 'SS1' 'SS2'};

ev.Table_location=[at.table.sixEarly;at.table.eight;at.table.sixLate];

switch remSbj
    case 'default'
        
    otherwise
         ev.Table_location(remSbj,:)=[];
end
% ev.Table_location=[at.table.eight;at.table.sixLate];
% ev.Table_location=[at.table.eight(1:2,:);at.table.sixLate(1:2,:)];



ev.cnd.Sbj= ev.Table_location.Properties.RowNames;  % FOR TESTING WHETHER IT CAN PROCESS ALL SHIFT DAY 
ev.Nsubjects=1:length (ev.cnd.Sbj);   % give every subjct the chance to become as a validation dataset



ev.separation.KSS_th=7; % Sandberg use 7 as the separation line

ev.FLoop.KdeLr_eta=1.08;
ev.FLoop.prm_th=2.18; %ev.FLoop.prm_th evaluate this range: 1:0.01:5;
ev.FLoop.lap_th= 5;  %ev.FLoop.lap_th  evaluate this range 1:1:19
ev.FLoop.sq_th=0.85;



ev.ppsPair.lap_th=ev.FLoop.lap_th;
ev.ppsPair.lr_th=ev.FLoop.KdeLr_eta;  % Diffrent ETA
ev.ppsPair.prm_th=ev.FLoop.prm_th;
ev.ppsPair.sq_th=ev.FLoop.sq_th;
ev.ppsPair.total_sbj=ev.Nsubjects;

[ev.position.lap_th,ev.position.KdeLr_eta, ev.position.prm_th,ev.position.sq_th,ev.position.total_sbj]=...
deal(1, 2, 3, 4, 5); % better assign awal2 macam ni, spaya x confuse downstream as, who column correspun what


[ev.position.Sen,ev.position.Spe,ev.position.SnSp,ev.position.Acc,...
 ev.position.Fmeasure,ev.position.Pre,ev.position.Rec]=deal(1,2,3,4,5,6,7); % better assign awal2 macam ni, spaya x confuse downstream as, who column correspun what

[ev.position.Kde0,ev.position.kde1,ev.position.PrmSq00,ev.position.PrmSq01,...
 ev.position.PrmSq10,ev.position.PrmSq11]=deal(1,2,3,4,5,6);



ev.cons.kde.alert=0.05;
ev.cons.kde.sleepy=0.05;




ev.dura.nMonteCarl=0; % How many repeatation to be repeated to create condition (e.g., missing data)
ev.dura.rndNoise_data=10; % Unit (Percentage)| In each the MC repeatation, how many percent of the data is missing?
end


