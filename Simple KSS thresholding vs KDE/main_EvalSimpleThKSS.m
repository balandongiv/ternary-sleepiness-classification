% {'01MA';'02DF';'03AJ';'04LY';'05NS';'06TS';'07PL';'08GB';'09CT';'10ML';'11NB';'12SA';'13JH';'14MR';'15QQ';'16SB';'17JY';'18CG';'19RG';'20MH';'21JF';'22CF';'23LL';'24MS'}
remSbj='01MA'; % 
% remSbj='default';
FileMatName='Result6E8H6L_04Sept2018_KDE_DiffTh_T1.mat';

[ev] =init_SBN (remSbj);

ev.state.sq_percent=SleepQuality_V1 (ev.table.sq_RAW);
ev=DatasetDivision_V1 (ev);
ppsPair=ev.ppsPair;
[sbj_ppsPair, ~]=forloop2ndgrid(ev.ppsPair); ev=rmfield(ev,'ppsPair');

[result]=EvalEachpair(ev,sbj_ppsPair);


Var_position=ev.position;
save(FileMatName,'result','sbj_ppsPair', 'Var_position');







function [Result]=EvalEachpair (ev,sbj_ppsPair)
Result=cell(1,size(sbj_ppsPair,1)); % Prelocate memory
c_sbj=0;

for f_pair=1:1:size(sbj_ppsPair,1) % For total n PVT,PRM,SQ combination pairs
    
            f_lap_th=sbj_ppsPair(f_pair,ev.position.lap_th);
            KdeLr_eta=sbj_ppsPair(f_pair,ev.position.KdeLr_eta);
            f_prm_th=sbj_ppsPair(f_pair,ev.position.prm_th);
            f_sq_th=sbj_ppsPair(f_pair,ev.position.sq_th);
            f_sbj=sbj_ppsPair(f_pair,ev.position.total_sbj);
            
    
    

    if f_sbj~= c_sbj  %     Variable clean up, just to make sure
        
        [TrainSet_loc,Validtn_Loc,DataSet_PVT,ev.PVT_train,ev.PVT_valid...
            ev.KSS_train,ev.KSS_valid, res_prm,ev.prdctn.PRM_valid,...
            state_sq,ev.prdctn.SQ_valid,ev.prdctn.SBN]=deal([]);
        
        [c_lap_th,c_prm_th,c_sq_th,c_sbj]=deal(0);
    else
    end
    
    

    
    if f_sbj~= c_sbj
        TrainSet_loc =single(cell2mat(ev.dataset.TrainingSet_cell(f_sbj)));
        Validtn_Loc=single(cell2mat(ev.dataset.ValidationSet_cell(f_sbj)));
    else
    end
    
    if f_lap_th ~= c_lap_th
        DataSet_PVT =ev.state.pvt(:,f_lap_th);
        ev.PVT_train= DataSet_PVT(TrainSet_loc);
        ev.PVT_valid= DataSet_PVT(Validtn_Loc);
        
        ev.KSS_train= ev.state.kss(TrainSet_loc);
        ev.KSS_valid= ev.state.kss(Validtn_Loc);



vec_th_sq=ev.separation.KSS_th.*(ones(length(ev.KSS_valid),1)); % create a vector of TSD_threshold, so we do element-by-element binary operation
res_KSS_th =bsxfun(@ge,ev.KSS_valid,vec_th_sq);  % If SQ is below th=0.85, the predic as poor SQ=1, and otherwise



        
        [res.sKss.Sen,res.sKss.Spe,res.sKss.SnSp,res.sKss.Acc, ...
            res.sKss.fmeas,res.sKss.Pre,res.sKss.Rec]...
                    =ConfMatrx_Performance(ev.PVT_valid,res_KSS_th);
                
                

    else
    end
    


Result{f_pair}=res;
    
c_lap_th=f_lap_th;  c_prm_th=f_prm_th;   c_sq_th=f_sq_th;    c_sbj=f_sbj;
end


end









function Sq_Percentg=SleepQuality (sq_RAW)
Sq_Percentg=(sq_RAW(:,1)+sq_RAW(:,2)+sq_RAW(:,3)+sq_RAW(:,4))./sq_RAW(:,5);
end


function ev=DatasetDivision (ev)
%% Validated with manual inspection in excel(23 Nov 2017)


%%% The column for the combine feature are, Row No of each of the feature,
%%% Sleep quality (%), KSS, PRM, and PVT.
combine_Feature=[(1:size(ev.state.kss,1))', ...
                ev.state.sq_percent,        ...
                ev.state.kss,               ...
                table2array(ev.table.prm),  ...
                table2array(ev.table.pvt)];

%% To understand, please refer the 4 lines below
% Dataset=sort (vertcat (Dataset_cell{:,:}));
% Logical_res=all((~isnan(combine_Feature)),2);
%  Validtn_Loc=intersect(find(Logical_res==1),Dataset); % Intersection between the NoN Nan row of features & the selected dataset (e.g., 6hE,6hL, 8h)
%  New=combine_Feature(Validtn_Loc,:);
%%% For compactness, use the single lines below. The output are same as
%%% those in combine feature. However, this one is the filtered version,
%%% which are, there is No NaN row, and only row that we interested>- For
%%% example row for group 6hE & 6HL, or row of 8h only, or any combination
%%% of the shift schedule protocol


Dataset_cell= table2array(ev.Table_location(:,ev.cnd.SD));
nDim=size(Dataset_cell,1);
%%%% The Var Dataset_F (f:Filtered), is a cleanup Dataset ( remove row that have either one or more NaN along the Row) 
Dataset_F=single(combine_Feature((intersect(find((all((~isnan(combine_Feature)),2))==1),sort (vertcat (Dataset_cell{:,:})))),:));
ExtractedData = (arrayfun(@(k)  Dataset_cell(k,:),1:size(Dataset_cell,1),'un',0))';
%%% UF:Unfiltered> location of the validation set in variable ValidationSet_cell_UF is in cell,  
ValidationSet_cell_UF = (arrayfun( @(m) cat(1, ExtractedData{m}{:}),1:numel(ExtractedData),'un',0));

ev.dataset.ValidationSet_cell= arrayfun(@(x)find(all(ismember(Dataset_F(:,1),ValidationSet_cell_UF{x}),2)),1:nDim,'un',0)';
Dataset_F_idx=(1:size(Dataset_F,1))'; % Total row size of the Dataset_F


ev.dataset.TrainingSet_cell = cellfun( @(m) setdiff(Dataset_F_idx,m),ev.dataset.ValidationSet_cell,'Un',0);

%%% Sleep quality (%), KSS, PRM, and PVT (True=sleepy, False=Alert)
[ev.state.sq_percent,ev.state.kss,ev.state.prm,ev.state.pvt]=...
    deal(Dataset_F(:,2),Dataset_F(:,3),Dataset_F(:,4),logical(Dataset_F(:,5:end))); 


fields={'Table_location','table','cnd'}; % Remove this field, since no usage of it anymore
ev=rmfield(ev,fields);
end
